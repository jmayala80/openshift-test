console.log("Hola 👋 Node.js.");

const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
    console.log(req.body);
    return res.json({ metodo: 'get' });
});

app.post('/', (req, res) => {
    console.log(req.body);
    return res.json({ metodo: 'post' });
});

app.listen(3000, () => {
    console.log("¡Aplicación de ejemplo escuchando en el puerto 3000!")
});
