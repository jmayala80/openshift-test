# Build & Deployment
## Create a build config using a Dockerfile in Gitlab
```
oc new-build https://gitlab.com/jmayala80/openshift-test.git --context-dir=/nodejs --name=nodejs-img-rest-api-01
# Use 'oc start-build' to trigger a new build
```
## Use 'oc start-build' to trigger a new build
```
oc start-build nodejs-img-rest-api-01
```
## Create a new application by specifying source code, a template, or an image.
```
oc new-app nodejs-img-rest-api-01 --name=nodejs-app-rest-api-02
```
## You can expose services to the outside world by executing the commands below:
```
oc expose svc/nodejs-app-rest-api-02
```
## Run 'oc status' to view your app (--suggest more details)
```
oc status
oc status --suggest
```
# Import image
```
oc import-image jamarcos02/nodejs:1.2 --confirm
```
```
oc new-app --docker-image=jamarcos02/nodejs:1.2
```

## Deployment
```
oc new-app nodejs:1.2
```
## Expose services
```
oc expose svc/nodejs
```

# Installing OpenShift Pipelines
 - https://docs.openshift.com/container-platform/4.5/pipelines/installing-pipelines.html#op-installing-pipelines-operator-using-the-cli_installing-pipelines

# Logging in to the Cluster
## Quien soy
```
oc whoami
```
## To login to the OpenShift cluster from the Terminal run:
```
oc login -u developer -p developer
```
## To execute a command as a cluster admin use the --as system:admin option to the command. For example:
```
oc get projects --as system:admin
```
## Create a new project
```
oc new-project myproject
oc project myproject
```
## Persistent Volume Claims
```
oc get pv --as system:admin
```
## Builder Images and Templates
You can see the list of what is available:
```
oc new-app -L
```
## Running Images as a Defined User
```
oc adm policy add-scc-to-user anyuid -z default -n myproject --as system:admin
```

# Developer CLI commands
 - https://docs.openshift.com/container-platform/4.5/cli_reference/openshift_cli/developer-cli-commands.html#new-app
 
# Otros temas futuros
## Importing an external Docker Image into Red Hat OpenShift repository
 - https://medium.com/@adilsonbna/importing-an-external-docker-image-into-red-hat-openshift-repository-c25894cd3199